$( document ).ready( documentReady );
$( window ).load( windowLoaded );

function documentReady ( jQuery ) {
				 if ($( ".fancybox-thumb" ).length > 0) {
				 				 //load fancybox
				 				 $(".fancybox-thumb").fancybox({
								 				     prevEffect	: 'none',
														 nextEffect	: 'none',
														 closeBtn : false,
														 helpers	: {
														 						 			title	: {	type: 'inside' },
																							buttons : {}
														 }
				 				 });
				 }

				 //set our onclick for login link
				 var $lnkLoginLogin = $( "#lnk-login-login" ); 
				 if ($lnkLoginLogin.length > 0) {
				 		$lnkLoginLogin.click(function(event) {
																								 event.preventDefault();
																								 toggleLoginVisible();
																								 });
						$( "#login-cancel" ).attr("onclick", "toggleLoginVisible();");
						$( "#login-submit" ).attr("onclick", "loginSubmit();");
						$( "#lnk-login-register" ).click(function (event) {
							 											 									event.preventDefault();
																											toggleRegisterVisible();
																											});
				 }
				 
				 
				 //load links for item page
				 if ($( "#prf-details" ).length > 0)
				 		setupItemLinks();

				 //load links for pulse page
				 if ($( "#prf-pulse" ).length > 0)
				 		setupProfileLinks();
}

function windowLoaded ( jQuery ) {
}

var $prfActive;
var $lnkActive;
var $prfPulse;
var $lnkPulse;
var $prfBidding;
var $lnkBidding;
var $prfSelling;
var $lnkSelling;
var $prfWatching;
var $lnkWatching;
var $prfFollowing;
var $lnkFollowing;
var $prfSharing;
var $lnkSharing;
var $prfHashing;
var $lnkHashing;
var $prfDetails;
var $lnkDetails;
var $prfBids;
var $lnkBids;
var $prfSeller;
var $lnkSeller;
var $prfWatchers;
var $lnkWatchers;
var $prfQnA;
var $lnkQnA;

function setupProfileLinks() {
				 // set up variables
				 $prfPulse = $( "#prf-pulse" );
				 $lnkPulse = $( "#lnk-pulse" );
				 $prfBidding = $( "#prf-bidding" );
				 $lnkBidding = $( "#lnk-bidding" );
				 $prfSelling = $( "#prf-selling" );
				 $lnkSelling = $( "#lnk-selling" );
				 $prfWatching = $( "#prf-watching" );
				 $lnkWatching = $( "#lnk-watching" );
				 $prfFollowing = $( "#prf-following" );
				 $lnkFollowing = $( "#lnk-following" );
				 $prfSharing = $( "#prf-sharing" );
				 $lnkSharing = $( "#lnk-sharing" );
				 $prfHashing = $( "#prf-hashing" );
				 $lnkHashing = $( "#lnk-hashing" );
				 
				 // set our onclicks
				 $lnkPulse.attr( "onclick", "activatePrfLnk( $prfPulse, $lnkPulse );");
				 $lnkBidding.attr( "onclick", "activatePrfLnk( $prfBidding, $lnkBidding );");
				 $lnkSelling.attr( "onclick", "activatePrfLnk( $prfSelling, $lnkSelling );");
				 $lnkWatching.attr( "onclick", "activatePrfLnk( $prfWatching, $lnkWatching );");
				 $lnkFollowing.attr( "onclick", "activatePrfLnk( $prfFollowing, $lnkFollowing );");
				 $lnkSharing.attr( "onclick", "activatePrfLnk( $prfSharing, $lnkSharing );");
				 $lnkHashing.attr( "onclick", "activatePrfLnk( $prfHashing, $lnkHashing );");
				 
				 // set our active variable
				 $prfActive = $prfPulse;
				 $lnkActive = $lnkPulse;
	 
	 			 // active pulse				 
				 activatePrfLnk( $prfPulse, $lnkPulse );
}

function toggleLoginVisible() {
				 var $loginBox = $( "#login-box" );
				 $loginBox.toggle();
}

function toggleRegisterVisible() {
				 var $registerBox = $( "#register-box" );
				 $registerBox.toggle();
}

function loginSubmit() {
				 // clear our login box
				 toggleLoginVisible();
				 // check our login
				 // if valid:
				 //   hide our login/register links
				 $( "#login" ).toggle();
				 //   update our logged in tag with username
				 //   show our logged in tag
				 $( "#logged-in" ).toggle();
				 //   enable our user menu pull-down
				 $( "#user-menu" ).toggle();
}

function activatePrfLnk( profile, lnk ) {
				 // hide our active content
				 $prfActive.css( "display", "none" );
				 // reset our active link
				 $lnkActive.css( "border-top", "0" );
				 // set our active content/link to what we are now
				 $prfActive = profile;
				 $lnkActive = lnk;
				 //  display our active content
				 $prfActive.css( "display", "block" );				 
				 // style our active link
				 $lnkActive.css( "border-top", "2px solid #0000ff" );
}

function setupItemLinks () {
				 // set up variables
				 $prfDetails = $( "#prf-details" );
				 $lnkDetails = $( "#lnk-details" );
				 $prfBids = $( "#prf-bids" );
				 $lnkBids = $( "#lnk-bids" );
				 $prfSeller = $( "#prf-seller" );
				 $lnkSeller = $( "#lnk-seller" );
				 $prfWatchers = $( "#prf-watchers" );
				 $lnkWatchers = $( "#lnk-watchers" );
				 $prfQnA = $( "#prf-qna" );
				 $lnkQnA = $( "#lnk-qna" );
				 $prfHashing = $( "#prf-hashing" );
				 $lnkHashing = $( "#lnk-hashing" );
				 
				 // set our onclicks
				 $lnkDetails.attr( "onclick", "activatePrfLnk( $prfDetails, $lnkDetails );");
				 $lnkBids.attr( "onclick", "activatePrfLnk( $prfBids, $lnkBids );");
				 $lnkSeller.attr( "onclick", "activatePrfLnk( $prfSeller, $lnkSeller );");
				 $lnkWatchers.attr( "onclick", "activatePrfLnk( $prfWatchers, $lnkWatchers );");
				 $lnkQnA.attr( "onclick", "activatePrfLnk( $prfQnA, $lnkQnA );");
				 $lnkHashing.attr( "onclick", "activatePrfLnk( $prfHashing, $lnkHashing );");
				 
				 // set our active variable
				 $prfActive = $prfDetails;
				 $lnkActive = $lnkDetails;
	 
	 			 // active pulse				 
				 activatePrfLnk( $prfDetails, $lnkDetails );
}