$( document ).ready( documentReady );
$( window ).load( windowLoaded );

function documentReady(jQuery) {
	//set our onclick for login link
	var $lnkLogin = $( "#lnk-login" ); 
	if ($lnkLogin.length > 0) {
		$lnkLogin.click(function(event) {
				event.preventDefault();
				toggleLoginVisible();
			});
		$( "#login-cancel" ).click(function(event) {
				event.preventDefault();
				toggleLoginVisible();
			});
		$( "#login-submit" ).click(function(event) {
				event.preventDefault();
				loginSubmit();
			});
		$( "#lnk-register" ).click(function (event) {
				event.preventDefault();
				toggleRegisterVisible();
			});
	}

}

function toggleLoginVisible() {
	var $loginBox = $( "#login-box" );
	$loginBox.toggle();
}

function toggleRegisterVisible() {
	var $registerBox = $( "#register-box" );
	$registerBox.toggle();
}

function loginSubmit() {
	// clear our login box
	toggleLoginVisible();
	// check our login
	// if valid:
	//   hide our login/register links
	$( "#login" ).toggle();
	//   update our logged in tag with username
	//   show our logged in tag
	$( "#logged-in" ).toggle();
}

function windowLoaded(jQuery) {
}