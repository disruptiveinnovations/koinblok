$( document ).ready( documentReady );
$( window ).load( windowLoaded );

function documentReady(jQuery) {
	var $div_nav = $("#nav");
	if ($div_nav.length > 0) {
		$.get("nav.html", function(data) {
			$div_nav.prepend(data);
		});
	}

	var $div_searchbar = $("#searchbar");
	if ($div_searchbar.length > 0) {
		$.get("searchbar.html", function(data) {
			$div_searchbar.prepend(data);
		});
	}

	var clickFxn = function() {
		if (typeof clickFxn.counter === 'undefined')
    		clickFxn.counter = 0;
		else
			clickFxn.counter++;
		alert("clicks: " + clickFxn.counter);
	}

	$("body").on("click", "#searchbar button", clickFxn);
}

function buildPopupForm(id, header, subHeader, formElements) {
	var htmlString = "<div id=\"" + id + "\" class=\"popup-form\">";
	htmlString += "<h3>" + escapeHtml(header) + "</h3>";
	htmlString += "<h4>" + escapeHtml(subHeader) + "</h4>";
	htmlSring += "</div>";
	for (var i = 0; i < formElements.length; i++) {
		var ele = formElements[i];
		/* build label (if applicable) */
		if (ele.eLabel) {
			htmlString += "<label for=\"" + id + ele.eId + "\">" + escapeHtml(ele.eName);
			if (ele.eRequired)
				htmlString += "<span class=\"req-star\">*</span>";
			htmlString +="</label>";
		}
		if (ele.eType == "text") {
			/* build our input */

		}
	}
}

function windowLoaded(jQuery) {
}

var entityMap = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#39;',
    "/": '&#x2F;'
};

function escapeHtml(string) {
	return String(string).replace(/[&<>"'\/]/g, function (s) {
		return entityMap[s];
	});
}